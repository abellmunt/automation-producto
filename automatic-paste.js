const tsvStringToArray = (data) => {
  const re = /(\t|\r?\n|\r|^)(?:"([^"]*(?:""[^"]*)*)"|([^\t\r\n]*))/gi
  const result = [[]]
  let matches
  while ((matches = re.exec(data))) {
    if (matches[1].length && matches[1] !== "\t") result.push([])
    result[result.length - 1].push(
      matches[2] !== undefined ? matches[2].replace(/""/g, '"').trim() : matches[3].trim()
    )
  }
  return result
}

jQuery(function() {
    jQuery(document).bind("paste", "#sortable-leadfield_properties", function(e){
        e.preventDefault();
        const text = e.originalEvent.clipboardData.getData("text");
        const arr = tsvStringToArray(text).slice(0, -1);

        let count = 0;
        arr.forEach(row => {
            if(row.length > 0) {
                if(!jQuery('#leadfield_properties_list_'+count+'_label').length) {
                    jQuery('#leadfield_properties_additem').click();
                    console.log('Hacer click');
                }
        
                if(row.length == 1)  {
                    jQuery('#leadfield_properties_list_'+count+'_label').val(row[0]);
                } else if (row.length > 1) {
                    jQuery('#leadfield_properties_list_'+count+'_label').val(row[0]);
                    jQuery('#leadfield_properties_list_'+count+'_value').val(row[1]);
                }
                count++;
            }
        });
    });
});
